# KWIKLY 
<!-- SPDX-License-Identifier: MPL-2.0 -->

Katalogue des données SNIIRAM-SNDS

::: tip
[Télécharger le Kwikly](../../files/Cnam/2020-06-18_CNAM_KWIKLY-Katalogue-Sniiram-SNDS-v2_MLP-2.0.xlsm) [CNAM- Version 2.0 - 2020-06-18 - MPL-2.0]
:::

Le **Kwikly** est un fichier Excel sur plusieurs onglets décrivant les variables des produits individuels bénéficiaires (type, taille, libellé) :
- DCIR
- PMSI
- DCIRS
- Cartographie
- Causes de décès
- Référentiel Bénéficiaires
- Référentiel Médicalisé

Le Kwikly permet de connaitre la présence des variables dans l’historique des produits depuis 2006.

Le Kwikly est un outil de clics rapides qui attend vos suggestions d’amélioration sur la boite <snds.cnam@assurance-maladie.fr>



