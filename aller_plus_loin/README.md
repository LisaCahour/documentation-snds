# Pour aller plus loin
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette section rassemble des ressources disponibles sur le SNDS.

## Documents téléchargeables

Les pages suivantes référencent les documents publiés sur ce site par chaque organisme : 
- [Cnam](documents_cnam/README.md) : [FAQ](documents_cnam/faq), [kwikly](documents_cnam/kwikly.md) et fiche sur la [pseudonymisation](documents_cnam/pseudonymisation.md)
- [GIS Epi-Phare](../aller_plus_loin/Epi-Phare.md)
- Ressources sur le SNDS [disponibles ailleurs en ligne](../aller_plus_loin/internet.md)
- Guide pour [accéder à la documentation du portail SNIIRAM](../aller_plus_loin/portail_sniiram.md)
- [Bibliographie](../aller_plus_loin/bibliographie.md)

## Outils en ligne

- Le [forum d'entraide](https://entraide.health-data-hub.fr) de la communauté des utilisateurs du SNDS
- Un [dictionnaire interactif](https://health-data-hub.shinyapps.io/dico-snds/) du SNDS, produit par la DREES
- Un [schema formalisé du SNDS](https://gitlab.com/healthdatahub/schema-snds)
qui alimente le dictionnaire interactif, et la partie **Tables** de cette documentation
- Une [cartographie des indicateurs de santé](http://dataviz.drees.solidarites-sante.gouv.fr/indicateurs_de_sante/) produite par la DREES, décrite [ici](../aller_plus_loin/cartographie_indicateurs.md)