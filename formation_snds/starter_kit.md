# Kit de démarrage avec les données de santé 
<!-- SPDX-License-Identifier: MPL-2.0 -->

Comment débuter avec les données de santé ?

Publié originalement sur le [site internet du Health Data Hub](https://www.health-data-hub.fr/) comme kit de démarrage avec les données. 

## De quelles données ai-je besoin pour mon projet ?

Le [système national des données de santé](https://documentation-snds.health-data-hub.fr/introduction/) (SNDS) est historiquement constitué des données dites "médico administratives": il s'agit des feuilles de soin, des données de facturation hospitalière et des causes médicales de décès. La loi du 24 juillet 2019 a élargi son périmètre à l'ensemble des données de santé donnant lieu à un remboursement de l'assurance maladie.

Lorsque je souhaite traiter ces données, je m'engage à :
- Communiquer au grand public une partie de mes résultats à l’issue de mon projet en application des obligations de transparence. Ces éléments de résultats seront rendus publics par le biais du répertoire des projets maintenus par le Health Data Hub
- Ne pas poursuivre l’une des finalités suivantes : la promotion en direction des professionnels de santé ou des établissements des produits de santé ; l’exclusion de garanties des contrats d’assurance ou la modification de cotisations ou de primes d’assurance pour un individu ou un groupe d’individus. Si je suis un industriel du produit de santé ou un assureur, je devrai être particulièrement attentif aux éléments de mon dossier permettant de garantir que je respecte ces dispositions.

Bientôt, le Health Data Hub mettra à ma disposition un catalogue de bases de données aisément accessibles. Dans l'attente, je peux être accompagné dans mes démarche auprès des responsables de données dont je souhaite utiliser une partie des données. Je peux trouver des exemples de bases de données [ici](https://epidemiologie-france.aviesan.fr).

Si je veux mobiliser plusieurs sources pour un même projet, c'est possible, mais il faudra que j'instruise le circuit d'appariement pour pouvoir faire circuler et chaîner les données en toute sécurité. Il existe de nombreux circuits possibles, des exemples sont illustrés [ici](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_dcf7ce522510405fa77707b7c56f375e.pdf).

## Qui sont mes partenaires ?

La réglementation en vigueur fait intervenir deux types de rôles : le responsable de traitement et le responsable de mise en œuvre. Je dois donc clairement identifier qui tient ces rôles dans mes partenaires car les responsabilités juridiques qui en découlent ne sont pas les mêmes.

Le responsable de traitement est à l'initiative de l'étude, tandis que le responsable de mise en oeuvre est chargé de l’exécuter. Il n'est pas rare que je sois les deux à la fois pour une même étude !

Ainsi, si je suis commanditaire ou promoteur de l’étude et je réalise moi-même le traitement : je suis responsable de traitement et responsable de mise en œuvre.
- Si je suis commanditaire de l’étude ou promoteur de l’étude mais c’est un tiers qui réalise le traitement : je suis responsable de traitement et le tiers est responsable de mise en œuvre.
- Si nous sommes plusieurs commanditaires de l’étude, la responsabilité de traitement doit donc être partagée entre nous.

Dans tous les cas de figure, les rôles respectifs de chacun des acteurs doivent être clairement établis via des conventions et/ou un contrat de sous-traitance.

## Quelles sont les démarches administratives à réaliser ?

En France, la réglementation est précise concernant la réutilisation des données personnelles de santé.

Premièrement, mon projet doit poursuivre une [**finalité d'intérêt public**](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_31f8fb3427d04105a12d6c85c7de9063.pdf). Ce n'est a priori pas incompatible avec une activité commerciale mais cela implique d'être explicité dans le protocole.

Deuxièmement, je dois faire **une demande d'autorisation auprès de la CNIL,** sauf dans des cas très précis où mon étude est éligible à des procédures dérogatoires ou simplifiées. Je peux dans ce cas réaliser mon projet dans le cadre d'une méthodologie de référence (MR). 

En contrepartie de cet accès facilité, je dois respecter certaines obligations :
- Faire un engagement de conformité sur le site internet de la [CNIL ](https://www.cnil.fr/fr/declarer-un-fichier)
- Enregistrer mon étude sur l[e répertoire public ](https://www.indsante.fr/fr/repertoire-public-des-etudes-realisees-sous-mr)tenu par le Health Data Hub

Je souhaite réutiliser des données de santé existantes et j’ai la capacité d’informer **individuellement** les personnes concernées avant la mise en œuvre mon étude ? Mon traitement peut être éligible à la [MR 004](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_40f57b1aa5e640b2bb717693a1a41893.pdf).

Il me sera toutefois interdit par exemple de : 
- Recueillir de données nominatives
- Procéder à un appariement avec une autre source
- Utiliser des données du SNDS non déjà chaînées dans une source de données tierce.

Je souhaite utiliser les données du PMSI. Deux MR peuvent être utilisées : 
- La [MR 005](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_d9d760a9c12d471095e221aff51136b1.pdf) si j’appartiens à un établissement de santé ou une fédération hospitalière
- La [MR 006](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_d9b58c97918249e6b60769b7ef229a5a.pdf) si j’appartiens à une structure produisant ou commercialisant des produits de santé et que la mise en œuvre de mon étude est assurée par un bureau d’études

Lorsque je ne suis pas éligible à une MR, je suis la procédure "classique".

Je dois donc constituer un dossier que je soumets au Health Data Hub qui joue le rôle de guichet unique de l'accès aux données de santé dans le cadre de cette procédure.

Ce dernier transmettra ma demande au comité éthique et scientifique pour les recherches, les études et les évaluations dans le domaine de la santé (CESREES) pour avis avant de la soumettre à la CNIL pour autorisation, le tout dans des délais encadrés.

Plus de renseignements sur le fonctionnement et les dates des prochaines réunions du CESREES, sont à trouver [ici](https://www.health-data-hub.fr/page/le-cesrees).

## Où vont être traitées mes données ?

Différentes options s'offrent à moi si je ne dispose pas de mon propre système d'information sécurisé pour traiter les données. Je peux recourir à un prestataire ou bien utiliser les plateformes du Health Data Hub ou de la CNAM.

Sur ces plateformes, un espace projet dédié m'est réservé. Je pourrai y traiter les données requises pour mon projet dès que mon projet sera autorisé, que j'aurai conventionné et aurai signé les CGU.

Sur la plateforme technologique du Health Data Hub, je peux faire du Python, du R, du Spark, ou encore du java. Sur la plateforme technologique de la CNAM, je dispose de SAS, Word, Excel et Powerpoint.

Pour le traitement du SNDS, ces plateformes doivent être conformes à un [référentiel de sécurité](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_d1febacb746b4123808561f989ca7594.pdf) spécifique. Si j'ai recours à la plateforme du Heallth Data Hub, mon correspondant pourra me fournir les pièces nécessaires pour le justifier au sein de mon dossier. Si je dispose de mon propre système d'information, il faudra que je produise et fournisse moi-même ces documents.

## Quelles pièces dois-je rassembler pour constituer mon dossier ?

Le dépôt de mon dossier s’effectue en ligne sur [une plateforme de dépôt](https://www.indsante.fr/fr/deposer-une-demande) de dossier. L’ ensemble de la procédure est dématérialisée. Mon dossier ne sera instruit que s'il est complet.

Je serai tenu informé par la plateforme de l’avancement de ma demande.

Mon dossier doit comprendre :
- [Un protocole scientifique type](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_59e9a5b2fd33461cb207fbdee24b5aaf.docx?dn=20200427-Template_protocole.docx) ([notice d'aide](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/8b518a_d087761b02574142ae80f4888d4f7ee4.pdf))
- [Un résumé type](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_2967180d6cbf4b58af9d40291712765e.docx?dn=20200427-Template_résumé.docx)
- La déclaration publique d'intérêt du [responsable de traitement](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_5eb56bc2fde64e878b5f5220c550a00c.pdf) et du [responsable de mise en œuvre](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_a8998058469f4fc29285fab1e61306ec.pdf)
- Le [formulaire](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_f5693851388b4162937d187719da6280.pdf) de demande d'autorisation de la CNIL pré-rempli 
- [Une lettre](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_53122d7e403f4866b9ff31e1da1fcc03.pdf) d’information aux personnes concernées
- L’avis du ou des comités scientifiques et/ou éthiques qui ont pu évaluer le projet et la composition de ce(s) comité(s), le cas échéant
- La liste des financeurs de l’étude
- Pour les données du SNDS, des éléments certifiant le [bon niveau de sécurité ](https://fee494fb-072e-49c6-a5ed-00cfc497e5db.filesusr.com/ugd/46ab38_d1febacb746b4123808561f989ca7594.pdf)du système d'information où elles seront traitées et une [analyse d'impact](https://www.cnil.fr/fr/outil-pia-telechargez-et-installez-le-logiciel-de-la-cnil).

## Comment aller plus loin ?

A toutes les étapes de ma démarche, je peux :
- Consulter le [répertoire des projets](https://health-data-hub.shinyapps.io/outil_visualisation/) pour avoir une idée de ce qui est réalisé à partir des données
- Interroger le [forum d'entraide](https://entraide.health-data-hub.fr/) pour échanger sur la faisabilité de mon étude
- Participer aux [matinales organisées](https://www.meetup.com/fr-FR/Health-Data-Hub/) en collaboration avec la CNAM pour avoir un premier niveau d'information sur le SNDS historique et aux [meet-up](https://www.meetup.com/fr-FR/Health-Data-Hub/) pour échanger avec tout type de porteurs de projet
- Me référer à la [documentation collaborative](https://documentation-snds.health-data-hub.fr/) ou utiliser un des programmes partagés par la communauté
- Adresser un mail à [hdh@health-data-hub.fr](mailto:hdh@health-data-hub.fr)